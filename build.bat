cd build/
del *.js
cd ../source/base
type square.js utils.js parse.js parse-html.js parse-utils.js > ../../build/a.js
cd ../core
type DataPath.js Html.js Value.js Control.js Reference.js Composite.js Error.js > ../../build/b.js
cd ../form
type TextField.js Count.js CharLimit.js Page.js List.js RssFeed.js > ../../build/c.js
cd ../flow
type Reference.js If.js AjaxSocket.js Closure.js > ../../build/d.js
cd ../
type model.js > ../build/z.js
cd ../build
type *.js > ../square.js
cd ../
copy square.js ..\GoEnrol\source\goenrol\assets\js\framework\square.js