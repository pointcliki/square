SQ.List = SQ.make(function() {

	this.tag = 'ul';

	// Super constructor
	SQ.Control.apply(this, arguments);
	
	this.change('source.*', this.updateElement, this);
	
	this.generateHtml();
}, {
	arguments: ['source', 'template', 'limit'],
	defaults: ['', '', 10],
	updateSource: function() {
		this.dom.empty();
		var i = 0, lim = this.limit.get();
		while (i < lim) {
			if (this.source.get(i) === undefined) break;
			var d = this.elementHtml(i);
			this.dom.append(d);
			i++;
		}
	},
	updateElement: function( i ) {
		// Replace element with new html if it exists
		var e = this.dom.eq(i);
		if (e.length) this.dom.eq(i).replaceWith(this.elementHtml(i));
	},
	elementHtml: function( i ) {
		var path = this.template.execute(this.source.subpath(i));
		path.tag = 'li';
		return path.html();
	},
	traits: 'List Control'
});