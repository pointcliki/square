SQ.Count = SQ.make(function() {
	// Super constructor
	SQ.Control.apply(this, arguments);
}, {
	arguments: ['input'],
	defaults: [0],
	generateHtml: function() {
		// Super method
		this.Control_generateHtml();
		
		this.dom.html(this.count || 0);
	},
	updateInput: function( key, value ) {
		this.data = this.input.get().length || 0;
		this.dom.html(this.data);
	},
	traits: 'Count Control'
});