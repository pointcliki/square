SQ.CharLimit = SQ.make(function() {
	// Super constructor
	SQ.Control.apply(this, arguments);
}, {
	traits: 'CharLimit Control',
	arguments: ['input', 'limit'],
	defaults: ['', 20],
	generateHtml: function() {
		// Super method
		this.Control_generateHtml();
		
		this.update();
	},
	update: function() {
		var text = this.input.get(), lim = this.limit.get();
		if ((text ? text.length : 0) > lim) {
			text = text.substring(0, text.substring(0, lim).lastIndexOf(' ')) + '...';
		}
		this.data = text;
		this.dom.html(text);
	}
});