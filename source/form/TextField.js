SQ.TextField = SQ.make(function() {
	// Super constructor
	SQ.Control.apply(this, arguments);
}, {
	arguments: ['input', 'defaultValue'],
	defaults: ['', ''],
	updateInput: function( key, value ) {
		if (!this.editing) {
			if (value && this.dom) this.dom.val(value).removeClass('defaultValue');
			else this.dom.val(this.defaultValue.get()).addClass('defaultValue');
		}
	},
	generateHtml: function() {
		this.tag = 'input';
		// Super method
		this.Control_generateHtml();
		// Default value
		var t = this, e = this.dom;
		e.val(this.input.get() || (e.addClass('defaultValue'), this.defaultValue.get()));
		// Focus
		e.bind('focus', function() {
			if (e.hasClass('defaultValue')) {
				e.val('').removeClass('defaultValue');
			}
		}).bind('blur', function() {
			if (!e.val().length) {
				e.val(t.defaultValue.get()).addClass('defaultValue');
			}
		});
		// Bind listeners
		e.bind('keyup keydown', function() {
			setTimeout(function() {
				t.editing = true;
				if (t.input.get() != e.val()) {
					t.input.put('', t.dom && t.dom.hasClass('defaultValue') ? '' : e.val());
				}
				t.editing = false;
			}, 0);
		});
	},
	traits: 'TextField Control'
});