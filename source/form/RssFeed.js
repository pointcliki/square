SQ.RssFeed = SQ.make(function() {

	this.tag = 'div';
	
	// Super constructor
	SQ.Control.apply(this, arguments);
	
}, {
	arguments: ['feed'],
	defaults: [''],
	generateHtml: function() {
		// Super
		this.Control_generateHtml();
		
		var dom = this.dom;
		dom.html('Loading...');
		// Fetch the RSS data
		SQ.RssFeed.fetch(this.feed.get(), function( data ) {
			var html = '<h2>' + data.title + '</h2>';
			var n = 0;
			for (var i in data.entries) {
				var e = data.entries[i];
				n++;
				if (n < 3) {
					html += '<a href="' + e.link + '" target="_blank">' + e.title + '</a>';
					html += '<p>' + e.contentSnippet + '</p>';
				}
			}
			dom.html(html);
		});
	},
	traits: 'RssFeed Control'
});

SQ.RssFeed.fetch =  function( url, callback ) {
	$.ajax({
		url: 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(url),
		dataType: 'json',
		success: function(data) {
			callback(data.responseData.feed);
		}
	});
};