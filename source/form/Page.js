SQ.Page = SQ.make(function() {

	this.tag = 'div';

	// Super constructor
	SQ.Control.apply(this, arguments);
	
	this.generateHtml();
	
	// Bind to changes in pages
	this.pages.change('*', function( key, value ) {
		if (key == this.get('name')) {
			var parse = SQ(this.parse(value));
			this.put('page', parse);
			this.dom.children().not('.old').empty().append(parse.html());
		}
	}, this);
	// Bind to change in location
	this.change('name', this.swapPage, this);
	
	// Set up navigation listener
	if (!document.location.hash) document.location.hash = this.homepage;
	else this.loadPage(decodeURIComponent(document.location.hash.substring(2)));
		
	$('body').on('click', 'a', function( e ) {
		var href = $(this).attr('href');
		// Navigate to href and check whether we should prevent default behaviour
		var type = href.substring(0, 1);
		var res = href.substring(1);
		if (type == '/') {
			// Update current page
			document.location.href = '#/' + res;
			return false;
			
		} else if (type == '!') {
			if (actions[res]) actions[res]();
			return false;
		} else if (type == '#') {
			return false;
		}
		return true;
	});
	
	var height = this.minHeight, t = this;
	
	// Wait for DOM refresh to measure height
	setInterval(function() {
		if (!t.dom.children().not('.old').length) return;
		var n = Math.max(t.minHeight, t.dom.children().not('.old').height());
		if (height != n) {
			height = n;
			t.dom.stop().animate({
				height: height
			}, 400);
		}
	}, 100);
	window.onhashchange = (function() {
		t.loadPage(decodeURIComponent(document.location.hash.substring(2)));
	});
}, {
	arguments: ['pages'],
	defaults: [{}],
	homepage: '/home/',
	route: 'pages/',
	label: 'page',
	minHeight: 300,
	loadPage: function( res ) {
		var t = this;
		if (t.pageChanging) return false;
		var done = function() {
			t.changePage(res);
		};
		if (SQ.Page.onUnload) SQ.Page.onUnload(res, done);
		else done();
		
		return false;
	},
	changePage: function( res ) {
		
		var ri = res.indexOf('/');
		if (ri != -1) {
			var href = res.substring(0, ri), data = res.substring(ri + 1);
		} else {
			var href = res;
		}
		
		this.put('name', href);
		this.put('info', data);
	},
	parse: function( data ) {
		var d = SQ.parse(data);
		if (SQ.parse.errors.length) return SQ.parse.errors[0];
		return d;
	},
	swapPage: function( key, value ) {
	
		this.pageChanging = true;
				
		// Fade out old page
		var old = this.dom.children('.' + this.label).addClass('old');
		old.animate({
			left: -1000,
			opacity: 0
		}, 400, function() {
			old.remove();
		});
		window.scrollTo(0, 0);
		
		delete SQ.Page.onUnload;
		delete SQ.Page.onLoad;
		
		var data = this.get('pages.' + value);
		var path = SQ(this.parse(data));
		this.put('page', path);
		
		// Populate page
		var e = $('<div class="' + this.label + '"></div>');
		path.appendTo(e);
		e.attr('name', 'page_' + value).css({
			opacity: 0,
			left: 1000
		}).animate({
			left: 0,
			opacity: 1
		}, 400);
		var t = this;
		setTimeout(function() {
			t.pageChanging = false;
		}, 400);
		// Execute scripts
		$('head').append(e.children('script'));
		
		// Append to content
		this.dom.append(e);
		setTimeout(function() {
			if (SQ.Page.onLoad) SQ.Page.onLoad();
		}, 1000);
	},
	pageChanging: false,
	traits: 'Page Control'
});