SQ.treeToSqm = function( tree, indent, escape ) {
	var tag = '', end = '';
	if (tree.traits == 'Html') {
		tag = tree.tag;
		
	} else if (tree.traits == 'Closure') {
		tag = '%';
		end = '%';
		
	} else if (tree.traits == 'Assign') {
		tag = '#' + tree.key;
		
	} else if (tree.traits == 'Composite') {
		end = ' ';
	
	} else if (tree.traits == 'Reference') {
		tag = '[';
		end = ']';

	} else if (tree.traits == 'Value') {
		if (!tree.data) return;
		// TODO: Fix hack quotes e.g. work out if they need quotes, and if so, escape quotes already in the value
		if (escape) return '"' + tree.data + '"';
		return tree.data;
		
	} else {
		tag = tree.traits;
	}
	var sqm = '';
	// Iterate through metadata
	for (var i in tree.metadata) {
		var front, meta = tree.metadata[i];
		if (i == 'id') {
			sqm += ' #' + SQ.treeToSqm(meta);
			continue;
			
		} else if (i == 'cls') {
			sqm += ' .'  + SQ.treeToSqm(meta).split(' ').join('.');
			continue;
			
		} else if (i == 'tag') {
			tag = meta.data;
			continue;
		}
		var c = SQ.treeToSqm(meta, 0, true);
		if (c) sqm += ' @' + i + '=' + c;
	}
	// Iterate through children
	i = 0;
	while (tree[i]) {
		var c = SQ.treeToSqm(tree[i]);
		if (c.isWhitespace()) {
			sqm += c;
		} else {
			sqm += ' ' + c;
		}
		i++;
	}
	if (tree.traits == 'Reference') sqm = sqm.substring(1);
	return '[' + tag + sqm + end + ']';
};
SQ.treeToHtml = function( tree ) {
	if (tree.traits == 'Html') {
		tag = tree.tag;
	} else {
		tag = 'span';
		if (!tree.metadata) tree.metadata = {};
		tree.metadata.name = tree.traits.split(' ')[0];
		tree.metadata.cls = tree.metadata.cls ? tree.metadata.cls + ' sq' : 'sq';
	}
	var e = $('<' + tag +'>' + '</' + tag + '>');
	for (var i in tree.metadata) {
		e.attr(i == 'cls' ? 'class' : i, tree.metadata[i]);
	}
	var i = 0;
	while (tree[i]) {
		if (tree[i].traits == 'Value') e.append(tree[i].data);
		else e.append(SQ.treeToHtml(tree[i]));
		i++;
	}
	return e;
};