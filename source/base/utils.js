String.prototype.trim = function(){return 
(this.replace(/^[\s\xA0]+/, "").replace(/[\s\xA0]+$/, ""))}

SQ.escapeRegExp = function( str ) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

String.prototype.startsWith = function(str) 
{return !!((this.match("^"+SQ.escapeRegExp(str)) || false).length)}

String.prototype.endsWith = function(str) 
{return !!((this.match(SQ.escapeRegExp(str)+"$") || false).length)}

String.prototype.isWhitespace = function(str) 
{return !!((this.match(/^\s+$/) || false).length)}

SQ.targets = function( key1, key2 ) {
	if (key1 == key2) return true;
	if (key1.endsWith('...')) return key2.startsWith(key1.substring(0, key1.length - 3));
	else if (key1.endsWith('.*')) return key2 && key1.substring(0, key1.length - 2) == key2.substring(0, key2.lastIndexOf('.'));
	else if (key1 == '*') return key2 && key2.indexOf('.') == -1;
	return false;
};
SQ.walk = function( key, callback ) {
	var front = '', i = key.indexOf('.');
	while (i != -1) {
		var part = key.substring(0, i);
		front += front ? '.' + part : part;
		key = key.substring(i + 1);
		var okay = callback(part, front, key);
		if (okay === false) return false;
		i = key.indexOf('.');
	}
	callback(key, front ? front + '.' + key : key, '');
	return true;
};

