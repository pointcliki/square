/**
 * Parse Square Markup (SQM) into a parse tree. Use SQ(tree) to instantiate the tree.
 * @param {String|DOM} An SQM string or jQuery DOM element that contains SQM
 * @returns {Structure} Returns the parse tree of the SQM as typed data.
 */
SQ.parse = function( dom ) {
	var sqm = dom.html ? dom.html() : dom;
	// Clear previous errors
	SQ.parse.errors = [];
	// Ensure we can parse the SQM
	if (typeof(sqm) != 'string') {
		SQ.parse.errors.push({
			0: {
				message: 'SQM must be parsed from a String or DOM element',
				type: 'NotAString'
			},
			traits: 'Error'
		});
		return false;
	}
	// First lex it
	var root = SQ.parse.lex(sqm);
	if (root) {
		// Use this to guide a parse
		var tree = SQ.parse.rich(sqm, root);
		tree.tag = dom[0].tagName || 'div';
		return tree;
	}
	return false;
};

/**
 * A list of parse errors that occurred during the previous call of SQ.parse
 */
SQ.parse.errors = [];

/**
 * Lex an SQM string to produce an abstract guide tree containing well-bracketed brackets and strings
 * @param {String} The SQM representation of the tag
 * @returns A guide tree to aid walking the SQM
 * @private
 */
SQ.parse.lex = function( sqm ) {
	// Root
	var root = {
		left: 0,
		right: sqm.length,
		children: []
	};

	// Line and column
	var line = 1, col = 0;
	
	var bracket, head = root;
	for (var i = 0; i < sqm.length; i++) {
		var c = sqm.charAt(i), tag;
		col++;
		if (bracket) {
			// TODO; Check for control sequence
			bracket = false;
			
			// Otherwise an escape bracket means don't read special meaning of following character
			//sqm = sqm.substring(0, i - 1) + sqm.substring(i);
			//i--;
			
		} else if (c == '\\') {
			bracket = true;
			
		} else if (c == '\n') {
			line++;
			col = 0;
		
		} else if (c == '\t') {
			col += 3;
			
		} else if (c == '[') {
			// Ignore inside string
			if (head && (head.token == '"')) continue;
			// New indent
			tag = {
				left: i,
				token: '[',
				parent: head,
				children: []
			};
			head.children.push(tag);
			head = tag;
		} else if (c == '"') {
			if (head.token == '"') {
				// Update head
				head.right = i + 1;
				// Reduce indent
				head = head.parent;
			} else {
				// New indent
				tag = {
					left: i,
					token: '"',
					string: true,
					parent: head
				};
				head.children.push(tag);
				head = tag;
			}
			
		} else if (c == ']') {
			// Ignore inside string
			if (head.token == '"') continue;
			// Check for error
			if (!head || !head.token || head.token.charAt(0) != '[') {
				SQ.parse.errors.push({
					0: {
						message: 'Too many closing brackets',
						at: 'Line ' + line + ', column ' + col,
						type: 'ClosedTwice'
					},
					traits: 'Error'
				});
				return false;
			}
			head.right = i + 1;
			head.farright = head.right;
			head.sqm = sqm.substring(head.left, head.right);
			// Check for subpath selector
			if (sqm.charAt(i + 1) == '.') {
				head.subpath = sqm.substring(head.right + 1).match(/^([A-Za-z._\-0-9]+)/)[0];
				head.farright += head.subpath.length + 1;
			}
			// Reduce indent
			head = head.parent;
		}
	}
	// Ensure that all the brackets are closed
	if (head != root) {
		SQ.parse.errors.push({
			0: {
				message: 'Brackets are not correctly closed',
				at: 'Line ' + line + ', column ' + col,
				type: 'NotClosed'
			},
			traits: 'Error'
		});
		return false;
	}
	return root;
};

/**
 * Parse an SQM string containing rich text (a list of text interspersed by SQM tags)
 * @param {String} The SQM representation of the tag
 * @param {Object} The lexer guide
 * @param {Boolean} True if a singleton element shouldn't be returned instead of its container
 * @returns The parse tree of the rich text
 * @private
 */
SQ.parse.rich = function( sqm, root, nomono ) {
	var tree = { traits: 'Composite' }, pointer = 0, child = 0;
	// Iterate through the children of the lexer guide tree
	for (var i in root.children) {
		// Get the current item
		var item = root.children[i];
		// Ignore strings in rich text as they don't mean anything special
		if (item.string) continue;
		// Check whether the item is a tag
		if (sqm.charAt(item.left - root.left) == '[') {
			/* If so, make sure we parse the text before it first
			 * As this is text since the last lexer item, we know it's not special
			 * and so can be treated as a text value. */
			if (item.left > pointer + root.left) {
				tree[child] = {
					data: sqm.substring(pointer, item.left - root.left),
					traits: 'Value'
				};
				child++;
			}
			// Parse the tag
			tree[child] = SQ.parse.tag(sqm.substring(item.left - root.left, item.farright - root.left), item);
			child++;
			// Increment the pointer to be at the end of the tag for the next iteration
			pointer = (item.farright || item.right) - root.left;
		}
	}
	// Parse the text that follows the final lexer item
	if (pointer != sqm.length) {
		tree[child] = {
			data: sqm.substring(pointer),
			traits: 'Value'			
		};
		child++;
	}
	// Return the tree, or the singleton child of the rich text if nomono is not set
	if (child == 1 && !nomono) return tree[0];
	return tree;
};

/**
 * Parse a square bracket tag of SQM
 * @param {String} The SQM representation of the tag
 * @param {Object} The lexer guide
 * @private
 */
SQ.parse.tag = function( sqm, root ) {

	// 1. First we classify the type of tag based on its first character
	
	var c = sqm.charAt(1), tag;
	
	// A closure
	if (c == '%') {
		if (root.right != root.farright) {
			SQ.parse.errors.push({
				0: {
					message: "Can't have a subpath of a closure",
					at: root.right,
					tag: sqm,
					type: 'BadSubpath'
				},
				traits: 'Error'
			});
			return false;
		}
		return {
			data: SQ.parse.rich(sqm.substring(3, sqm.length	 - 3), SQ.parse.subtree(root, root.left + 3, root.right - 3)),
			traits: 'Closure'
		};
		
	// A value type (object or string)
	} else if (c == '{' || c == '"') {
		var data;
		eval('data = ' + sqm.substring(1, sqm.lastIndexOf('}') + 1) + ';');
		return {
			data: data,
			traits: 'Value'
		}
	
	// An assignment
	} else if (c == '#') {
		// TODO: make name adaptable - string or tag
		var match = sqm.match(/^..([0-9A-Za-z_\-.]+)/);
		// Ensure the tag has a name
		if (match) pointer = match[0].length + 1;
		console.log(sqm.substring(pointer, sqm.length - 1));
		return {
			key: sqm.substring(2, pointer ? pointer - 1: sqm.length - 1),
			data: SQ.parse.rich(sqm.substring(pointer, sqm.length - 1), SQ.parse.subtree(root, root.left + pointer, root.right - 1)), // Allow mono assignment
			traits: 'Assign'
		};
		
	// A reference target
	} else if (c == '$') {
		tag = SQ.parse.rich(sqm.substring(2, sqm.length - 1), SQ.parse.subtree(root, root.left + 2, root.right - 1), true);
		tag.traits = 'Reference';
		return tag;
		
	// Whitespace indicates the tag is parentheses for rich text
	} else if (c == ' ' || c == '\t' || c == '\n') {
		return SQ.parse.rich(sqm.substring(2, sqm.length - 1), SQ.parse.subtree(root, root.left + 2, root.right - 1), true);
	}
	
	// 2. If the tag isn't special then we find its name
	
	tag = {};
	var match = sqm.match(/^.([0-9A-Za-z_\-]+)/);
	// Ensure the tag has a name
	if (match) pointer = match[0].length + 1;
	else {
		SQ.parse.errors.push({
			0: {
				message: 'Badly named tag',
				at: root.left,
				tag: sqm,
				type: 'BadTag'
			},
			traits: 'Error'
		});
		return false;
	}
	// Assign the name as a trait of the tag
	tag.traits = sqm.substring(1, pointer ? pointer - 1: sqm.length - 1);
	
	// 3. Check for special types of arguments
	
	// Check for no arguments
	if (!pointer) return tag;
	
	// Check for script as its arguments are raw text rather than rich text
	if (tag.traits == 'script') {
		tag[0] = {
			data: sqm.substring(pointer, sqm.length - 2),
			traits: 'Value'
		}
		return tag;
	}
	
	// 4. Now we parse the arguments
	
	var i = 0, child, end;
	
	// Scroll pointer through arguments
	while (pointer < sqm.length - 1) {
	
		// Get the character at the head of the argument
		c = sqm.charAt(pointer);
		
		// If this is whitespace increment the pointer
		if (c == ' ' || c == '\n' || c == '\t') {
			pointer++;
			continue;
		}
		
		// A tag argument
		if (c == '[') {
			// Find the tag item from the lexer guide
			child = SQ.parse.childAt(root, pointer);
			if (!child || child.token != '[') {
				SQ.parse.errors.push({
					0: {
						message: "Can't find descriptor for bracket",
						at: root.left + pointer,
						tag: sqm,
						type: 'BadBracket'
					},
					traits: 'Error'
				});
				return false;
			}
			// Parse the tag and add it as a child argument
			tag[i] = SQ.parse.tag(sqm.substring(pointer, child.right - root.left), child);
			i++;
			pointer = child.farright - root.left;
			continue;
		
		// Metadata (a classname or id)
		} else if (c == '.' || c == '#') {
			pointer++;
			// TODO: Make nicer with regex
			// Get the string representing the data
			var str = sqm.substring(pointer);
			end = Math.min(str.indexOf(']') == -1 ? Infinity : str.indexOf(']'), str.indexOf('\n') == -1 ? Infinity : str.indexOf('\n'), str.indexOf(' ') == -1 ? Infinity : str.indexOf(' '), str.indexOf('.') == -1 ? Infinity : str.indexOf('.')) + pointer;
			if (!tag.metadata) tag.metadata = {};
			// For a class
			if (c == '.') {
				if (!tag.metadata.cls) tag.metadata.cls = {
					data: sqm.substring(pointer, end),
					traits: 'Value'
				}
				else tag.metadata.cls.data += ' ' + sqm.substring(pointer, end);
				
			// For an id
			} else {
				tag.metadata.id = {
					data: sqm.substring(pointer, end),
					traits: 'Value'
				}
			}
			// Move pointer to the end of the declaration
			pointer = end;
		
		// Custom metadata (any other HTML attributes)
		} else if (c == '@') {
		
			end = sqm.substring(pointer).indexOf('=') + pointer;
			var attr = sqm.substring(pointer + 1, end);
			pointer = end + 1;
			var c2 = c2 = sqm.charAt(pointer);
			
			// As a string
			if (c2 == '"') {
				if (!tag.metadata) tag.metadata = {};
				child = SQ.parse.childAt(root, pointer);
				tag.metadata[attr] = {
					data: sqm.substring(child.left + 1 - root.left, child.right - 1 - root.left),
					traits: 'Value'
				}
				pointer = child.right - root.left;
			
			// As a tag
			} else if (c2 == '[') {
				if (!tag.metadata) tag.metadata = {};
				child = SQ.parse.childAt(root, pointer);
				tag.metadata[attr] = SQ.parse.tag(sqm.substring(pointer, child.right - root.left), SQ.parse.subtree(root, child.left, child.right));
				pointer = child.farright - root.left;
			
			// As raw text without spaces
			} else {
				if (!tag.metadata) tag.metadata = {};
				var str = sqm.substring(pointer);
				// TODO: make regex
				end = Math.min(str.indexOf(']') == -1 ? Infinity : str.indexOf(']'), str.indexOf('\n') == -1 ? Infinity : str.indexOf('\n'), str.indexOf(' ') == -1 ? Infinity : str.indexOf(' ')) + pointer;
				tag.metadata[attr] = {
					data: sqm.substring(pointer, end),
					traits: 'Value'
				};
				pointer = end + 1;
			}
		
		// A string argument
		} else if (c == '"') {
			// Find tag
			child = SQ.parse.childAt(root, pointer);
			if (!child.string) {
				SQ.parse.errors.push({
					0: {
						message: "Can't find descriptor for string",
						at: root.left + pointer,
						tag: sqm,
						type: 'BadString'
					},
					traits: 'Error'
				});
				return false;
			}
			tag[i] = {
				data: sqm.substring(pointer + 1, child.right - root.left - 1),
				traits: 'Value'
			}
			i++;
			pointer = child.right - root.left;
			continue;
			
		} else {
			// Escape leading bracket
			if (c == '\\') pointer++;
			// Parse rich text
			var rich = SQ.parse.rich(sqm.substring(pointer, sqm.length - 1), SQ.parse.subtree(root, pointer + root.left, sqm.length - 1 + root.left), true), j = 0;
			// Concatenate
			while (rich[j]) {
				tag[i] = rich[j];
				i++;
				j++;
			}
			// Check if the bracket is followed by a subpath director
			if (root.subpath) {
				return {
					path: tag,
					key: root.subpath,
					traits: 'SubPath'
				}
			}
			return tag;
		}
	}
	// Check if the bracket is followed by a subpath director
	if (root.subpath) {
		return {
			path: tag,
			key: root.subpath,
			traits: 'SubPath'
		}
	}
	return tag;
};

/**
 * A helper function to return the immediate child of the current guide at the position indicated
 * @param {Object} The lexer guide
 * @param {Number} The offset to look for a child
 * @returns {Object} Another guide: the lexer's child at position given
 * @private
 */
SQ.parse.childAt = function( root, val ) {
	for (var j in root.children) {
		if (root.children[j].left == val + root.left) return root.children[j];
	}
	return false;
};

/**
 * A helper function to return the lexing artefacts in a particular range
 * @param {Object} The root lexer guide
 * @param {Number} The leftmost offset to look for descendants
 * @param {Number} The rightmost offset to look for descendants
 * @returns {Object} Another guide containing the lexers descendants in the range provided
 * @private
 */
SQ.parse.subtree = function( root, left, right ) {
	var children = [];
	for (var j in root.children) {
		//if (root.children[j].left == left && root.children[j].right == right) return root.children[j];
		if (root.children[j].left >= left && root.children[j].right <= right) children.push(root.children[j]);
		else if (root.children[j].left < right && root.children[j].right > left) children = children.concat(SQ.parse.subtree(root.children[j], left, right));
	}
	return {
		left: left,
		right: right,
		children: children
	}
};