SQ.parseHtml = function( html ) {
	// Check for text
	if (html[0].nodeType == 3) return {
		data: html[0].data,
		traits: 'Value'
	}
	// Otherwise parse as a tag
	var tree = {};
	// Check for Square specific html
	if (html.hasClass('sq')) {
		// The trait is the name of the HTML tag
		tree.traits = html.attr('name');
		html.removeClass('sq');
		html.removeAttr('name');
		if (!html.attr('class')) html.removeAttr('class');
		
	} else if (html.hasClass('sq-ref')) {
		tree.traits = 'Reference';
		tree.tag = html[0].tagName.toLowerCase();
		html.removeClass('sq-ref');
		if (!html.attr('class')) html.removeAttr('class');
		
	} else if (html.hasClass('sq-closure')) {
		tree.traits = 'Closure';
		tree.tag = html[0].tagName.toLowerCase();
		html.removeClass('sq-closure');
		if (!html.attr('class')) html.removeAttr('class');
		
	} else {
		// Set the trait of the square tag to the HTML tag name
		tree.traits = (html[0].tagName || '').toLowerCase();
	}
	for (var i in html[0].attributes) {
		var item = html[0].attributes.item(i);
		if (!item) continue;
		if (!tree.metadata) tree.metadata = {};
		tree.metadata[item.name == 'class' ? 'cls' : item.name] = {data: item.value, traits: 'Value'};
	}
	var n = 0;
	html.contents().each(function() {
		// Check for metadata
		if ($(this).hasClass('sq-attr')) {
			$(this).removeClass('sq-attr');
			if (!tree.metadata) tree.metadata = {};
			tree.metadata[$(this).attr('name')] = SQ.parseHtml($(this).removeAttr('name'));
		} else {
			var parse = SQ.parseHtml($(this));
			// Check for whitespace
			if (parse.traits != 'Value' || parse.data.trim() !== '') {
				tree[n] = parse;
				n++;
			}
		}
	});
	return tree;
};