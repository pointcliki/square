SQ = function( model, scope ) {
	if (!model.traits) return model;
	var obj, traits = model.traits, scope = scope || SQ.model;
	// Get args
	var args = [], i = 0;
	while (model[i]) {
		args.push(SQ(model[i], scope));
		i++;
	}
	// Get the primary trait of the Structure
	var ind = traits.indexOf(' '), type = ind == -1 ? traits : traits.substring(0, ind);
	
	// Check for a special trait
	if (type == 'SubPath') {
		return SQ(model.path, scope).subpath(model.key);
		
	} else if (type == 'Assign') {
		obj = SQ(model.data, scope);
		SQ.model.graft(model.key, obj);
		return obj;
		
	} else {
		obj = {data: model.data, traits: traits, metadata: {}};
		// Parse metadata
		for (var i in model.metadata) {
			obj.metadata[i] = SQ(model.metadata[i], scope);
		}
		// Set scope
		SQ.scope.path = scope;
		// Get constructor for type or default constructor
		var cons = SQ[type];
		if (!cons) {
			// Check for missing type
			if (model.traits.charAt(0) == model.traits.charAt(0).toUpperCase()) console.warn('Missing type ' + model.traits);
			obj.metadata.tag = new SQ.DataPath(model.traits);
			cons = SQ[SQ.options.defaultConstructor];
		}
		// Create object
		obj.__proto__ = cons.prototype;
		cons.apply(obj, args);
	}
	return obj;
};

SQ.scope = {};

SQ.options = {
	convertSqm: '.sqm',
	convertHtml: '.sq-html',
	defaultConstructor: 'Html'
};

SQ.make = function( cons, proto ) {
	if (!proto) {
		proto = cons;
		cons = function() {this.data = {}};
	}
	var traits = proto.traits ? proto.traits.split(' ') : ['DataPath'];
	// Create a consturct method
	if (!proto.construct) proto.construct = cons;
	// Get mixin classes
	for (var i in traits) {
		var trait = traits[i], cls = SQ[trait];
		if (cls) {
			// Copy over constructor
			proto[trait] = cls;
			// Copy over methods that are being overridden
			for (var j in cls.prototype) {
				if (j == 'traits') continue;
				if (proto[j]) {
					proto[trait + '_' + j] = cls.prototype[j];
				}
				else proto[j] = cls.prototype[j];
			}
		}
	}
	cons.prototype = proto;
	return cons;
};

$(function() {
	if (SQ.options.convertSqm) {
		$(SQ.options.convertSqm).each(function() {
			var e = $(this), parse = SQ.parse(e);
			if (SQ.parse.errors.length) parse = SQ.parse.errors[0];
			e.replaceWith(SQ(parse).html().hide().fadeIn(500));
		});
	}
	if (SQ.options.convertHtml) {
		$(SQ.options.convertHtml).each(function() {
			var e = $(this);
			e.replaceWith(SQ(SQ.parseHtml(e)).html());
		});
	}
});

