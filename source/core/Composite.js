SQ.Composite = SQ.make(function() {
	// Super constructor
	this.Html.apply(this, arguments);
	this.tag = 'div';
}, {
	get: function( key ) {
		if (key) return this[key].get();
		var c = '', i = 0;
		while (this[i]) {
			c += this[i].get();
			i ++;
		}
		return c;
	},
	traits: 'Composite Html'
});