SQ.Control = SQ.make(function() {
	this.data = {};
	
	for (var i in this.arguments) {
		var name = this.arguments[i], arg = arguments[i] || new SQ.DataPath(this.defaults[i]);
		// Copy arguments to field
		this[name] = arg;
		this.graft(name, arg);
	}
	this.bind.apply(this, arguments);
	if (SQ.Control.options.cls) {
		var text = SQ.Control.options.cls + this.traits.split(' ')[0];
		if (!this.metadata) this.metadata = {};
		if (this.metadata.cls) text = this.metadata.cls.get() + ' ' + text;
		this.metadata.cls = new SQ.DataPath(text);
	}
	if (this.metadata && this.metadata.id) {
		SQ.model.graft(this.metadata.id.get(), this);
	}
	for (var i in this.metadata) {
		this.graft(i, this.metadata[i]);
	}
}, {
	bind: function() {
		for (var i in this.arguments) {
			var name = this.arguments[i], arg = arguments[i];
			if (arg) {
				// Capitalize name
				var caps = name, lead = name.substring(0, 1).charCodeAt(0);
				if (lead >= 97 && lead <= 122) {
					caps = String.fromCharCode(lead - 32) + name.substring(1);
				}
				// Bind argument listener
				if (this['update' + caps]) {
					arg.change(this['update' + caps], this);
				}
				if (this.update) arg.change(this.update, this);
			}
		}
	},
	release: function() {
		for (var i in this.arguments) {
			var name = this.arguments[i], arg = arguments[i];
			// Capitalize name
			var caps = name, lead = name.substring(0, 1).charCodeAt(0);
			if (lead >= 97 && lead <= 122) {
				caps = String.fromCharCode(lead - 32) + name.substring(1);
			}
			// Unbind argument listener
			if (this['update' + name]) arg.release(this['update' + name]);
		}
	},
	traits: 'Control Html'
});
SQ.Control.options = {
	cls: 'sq-'
};