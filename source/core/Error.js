SQ.Error = SQ.make(function( model ) {
	// No super constructor
	this.data = model;
}, {
	generateHtml: function() {
		if (this.dom) return;
		
		// Set tag
		var e = $('<div class="sq-Error"></div>');
		var title = $('<div class="title"></div>');
		title.append('Error ').append(this.data.type);
		e.append(title).append(this.data.message);
		
		for (var i in this.data) {
			if (i == 'type' || i == 'message') continue;
			var field = $('<div class="field"></div>');
			field.append('<b>' + i + '</b> ').append(this.data[i]);
			e.append(field);
		}
		
		this.dom = e;
	},
	traits: 'Error Html'
});