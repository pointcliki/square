SQ.Html = SQ.make(function() {
	// No super constructor as we handle the arguments ourselves
	// Get all the arguments
	for (var i = 0; i < arguments.length; i++) this[i] = arguments[i];
}, {
	generateHtml: function() {
		if (this.dom) return;
		
		// Set tag
		this.tag = this.tag || (this.metadata && this.metadata.tag ? this.metadata.tag.get() : 'span');
		
		var t = this, e = $('<' + t.tag + '></' + t.tag + '>'), i = 0;
		e[0].square = this;
		// Add children
		while (t[i]) {
			if (t[i].html) e.append(t[i].html());
			i++;
		}
		if (t.metadata) {
			for (var i in t.metadata) {
				if (i == 'tag') continue;
				e.attr(i, t.metadata[i].get());
				t.metadata[i].change('...', t.updateMetadata, this);
			}
		}
		e.attr('class', e.attr('cls')).removeAttr('cls');
		t.dom = e;
	},
	bind: function() {
		if (this.bound) return;
		this.bound = true;
		var i = 0;
		while (this[i]) {
			this[i].change(this.updateChild, this);
			i++;
		}
	},
	updateChild: function( key, value, path ) {
		if (key) key = '.' + key;
		var i = 0;
		while (this[i]) {
			if (this[i] == path) {
				this.trigger(i + key, value);
				return;
			}
			i++;
		}
	},
	updateMetadata: function( key, value, path ) {
		if (this.dom) {
			for (var i in this.metadata) {
				if (this.metadata[i] == path) {
					this.dom.attr(i, value);
					return;
				}
			}
		}
	},
	change: function( key, value, call ) {
		// By default don't bind arguments
		this.bind();
		// Super
		this.DataPath_change(key, value, call);
	},
	html: function() {
		if (!this.dom) this.generateHtml();
		return this.dom;
	},
	prependTo: function( e ) {
		if (typeof(e) == 'string') e = $(e);
		e.prepend(this.html());
		return this;
	},
	appendTo: function( e ) {
		if (typeof(e) == 'string') e = $(e);
		e.append(this.html());
		return this;
	},
	release: function() {
		for (var i in this.metadata) {
			this.metadata[i].release(this.updateMetadata);
		}
	},
	value: function() {
		var s = '';
		// Add children
		while (t[i]) {
			s += t[i].value();
			i++;
		}
		return s;
	},
	traits: 'Html DataPath text/html text/value'
});