SQ.DataPath = SQ.make(function( model ) {
	if (model) this.data = model;
	//this.listeners = [];		// Listeners of data changes bound to this data path
	//this.parents = [];		// Data paths above that should be notified of an update
	//this.children = [];		// Data paths below that should be notified of an update
	//this.grafts = {};		// Data paths that are used to access particular data in the model
	//this.triggering = {};
}, {
	addChild: function( key, path ) {
		if (!this.children) this.children = [];
		this.children.push({key: key, path: path});
	},
	removeChild: function( key, path ) {
		var found = false, i;
		for (i in this.children) {
			if (this.children[i].key == key && this.children[i].path == path) {
				found = true;
				break;
			}
		}
		if (found) this.children.splice(i, 1);
	},
	addParent: function( key, path ) {
		if (!this.parents) this.parents = [];
		this.parents.push({key: key, path: path});
	},
	removeParent: function( key, path ) {
		var found = false, i;
		for (i in this.parents) {
			if (this.parents[i].key == key && this.parents[i].path == path) {
				found = true;
				break;
			}
		}
		if (found) this.parents.splice(i, 1);
	},
	subpath: function( key ) {
		var path, graft;
		if (!key) {
			return this;
			//path = this.construct(this.data);
		} else {
			var t = this, data = t.data;
			SQ.walk(key + '', function( part, front, end ) {
				// Check for graft
				if (t.grafts && t.grafts[front]) {
					graft = t.grafts[front].subpath(end);
					return false;
				}
				// Drill down
				data = data[part];
				if (data === undefined) return false;
			});
			if (graft) return graft;
			path = new t.construct(data);
		}
		path.addParent(key, this);
		path.placeholder = true;
		this.addChild(key, path);
		return path;
	},
	anchor: function() {
		delete this.placeholder;
	},
	change: function( key, callback, self ) {
		if (typeof(key) != 'string') {
			self = callback;
			callback = key;
			key = '';
		}
		// Add listener
		if (!this.listeners) this.listeners = [];
		this.listeners.push({key: key + '', callback: callback, self: self || this});
	},
	release: function( key, callback ) {
		if (!key) {
			delete this.listeners;
			for (var i in this.parents) this.parents[i].path.removeChild(this.parents[i].key, this);
		} else {
			for (var i in this.listeners) {
				var li = this.listeners[i];
				if (li.key == key && callback == li.callback) {
					this.listeners.splice(i, 1);
					return;
				}
			}
		}
		// TODO remove key
		// TODO remove namespace
	},
	get: function( key ) {
		if (!key) return this.data;
		var t = this, data = t.data;
		SQ.walk(key + '', function( part, front, end ) {
			// Check for graft
			if (t.grafts && t.grafts[front]) {
				data = t.grafts[front].get(end);
				return false;
			}
			// Drill down
			data = data[part];
			if (data === undefined) return false;
		});
		return data;
	},
	put: function( key, value ) {
		var graft = false;
		if (key) {
			var t = this, data = t.data;
			SQ.walk(key + '', function( part, front, end ) {
				// Check for graft
				if (t.grafts && t.grafts[front]) {
					t.grafts[front].put(end, value);
					graft = true;
					return false;
				}
				if (!end) {
					if (!data) data = {};
					data[part] = value;
					return false;
				}
				// Drill down
				if (!data) data = {};
				data = data[part];
			});
		}
		if (!graft) this.trigger(key, value);
	},
	delete: function( key ) {
		// TODO
		delete this.grafts[key];
	},
	graft: function( key, path ) {
		if (!this.grafts) this.grafts = {};
		if (this.graft[key]) {
			console.warn('Already grafted a path to ' + key);
			return;
		}
		if (!path && typeof(key) == 'object') {
			path = key;
			key = '';
		}
		if (path) {
			// TODO: handle wildcards
			var g = path.get();
			if (g !== undefined) this.put(key, g);
			this.grafts[key] = path;
			this.addChild(key, path);
			path.addParent(key, this);
			
		} else {
			var bag = [];
			for (var i in this.grafts) {
				if (SQ.targets(key, i)) bag.push(this.grafts[i]);
			}
			return bag;
		}
	},
	ungraft: function( key ) {
		// TODO: handle wildcards
		this.delete(key);
	},
	trigger: function( key, value, force, event ) {
		// TODO: place type of event in event object use this to check to fire listeners
		// Check we're not already triggering this value
		if (!this.triggering) this.triggering = {};
		if (this.triggering[key]) return;
		this.triggering[key] = true;
		// Update the data if forced
		if (!key) {
			this.data = value;
		} else if (force) {
			this.put(key, value);
		}
		// If the key is root then we need to check whether we're anchored
		if (!key) {
			// Detatch from parents if anchored
			if (!this.placeholder) {
				for (var i in this.parents) {
					this.parents[i].path.removeChild(this.parents[i].key, this);
					this.removeParent(this.parents[i].key, this.parents[i].path);
				}
			}
		}
		// Fire listeners
		for (var i in this.listeners) {
			if (SQ.targets(this.listeners[i].key, key + '')) {
				var call = this.listeners[i].callback;
				call.apply(this.listeners[i].self, [key, value, this]);
			}
		};
		// Trigger parents
		for (var i in this.parents) {
			var pkey = this.parents[i].key;
			if (pkey) pkey += (key ? '.' : '') + key;
			else pkey = key;
			this.parents[i].path.trigger(pkey, value, !key);
		}
		// Trigger children
		for (var i in this.children) {
			var cpath = this.children[i].path, ckey = this.children[i].key + '';
			
			// If the model contains the child
			if (ckey.startsWith(key + '.')) {
			
				// If anchored then we no longer watch the child
				if (cpath.anchored) {
					cpath.removeParent(ckey, this);
					this.removeChild(ckey, cpath);
					
				} else {
					var subvalue = value;
					SQ.walk(key ? ckey.substring(key.length + 1) : ckey, function( part, front, end ) {
						if (subvalue == undefined) return false;
						subvalue = subvalue[part];
					});
					// Update the contents of the child
					cpath.trigger('', subvalue, true);
				}
			
			// If the key is inside the child's model
			} else if (key.startsWith(ckey + '.')) {
				// Trigger an update for the child
				cpath.trigger(ckey ? key.substring(ckey.length + 1) : key, value);
			}
		}
		// No longer triggering
		delete this.triggering[key];
	},
	hasTrait: function( trait ) {
		var arr = this.traits.split(' ');
		for (var i in arr) if (arr == trait) return true;
		return false;
	},
	traits: 'DataPath'
});