SQ.Value = SQ.make(function() {
	// Super constructor
	this.DataPath.apply(this, arguments);
}, {
	json: function() {
		return JSON.stringify(this.data);
	},
	html: function() {
		return this.data;
	},
	value: function() {
		return this.data;
	},
	prependTo: function( e ) {
		if (typeof(e) == 'string') e = $(e);
		e.prepend(this.html());
		return this;
	},
	appendTo: function( e ) {
		if (typeof(e) == 'string') e = $(e);
		e.append(this.html());
		return this;
	},
	traits: 'Value DataPath text/html text/json'
}, 'DataPath');