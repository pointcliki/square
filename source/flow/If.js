SQ.If = SQ.make(function() {
	// Super constructor
	SQ.Control.apply(this, arguments);
}, {
	arguments: ['condition', 'high', 'low'],
	defaults: ['', 'true', 'false'],
	update: function() {
		if (this.dom) {
			var v = this.condition.get() ? this.high : this.low;
			if (this.dom) this.dom.empty().append(v.html());
		}
	},
	generateHtml: function() {
		// Super method
		this.Control_generateHtml();
		var v = this.condition.get() ? this.high : this.low;
		this.dom.empty();
		if (v && v.html) this.dom.append(v.html());
		else console.warn('[If] Child has no displayable Html', this, v);
	},
	traits: 'If Control'
});

