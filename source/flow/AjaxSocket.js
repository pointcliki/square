SQ.AjaxSocket = SQ.make(function() {
	// Super constructor
	SQ.Control.apply(this, arguments);
	var t = this;
	// Graft on worker
	var worker = new (SQ.make({
		get: function( key ) {
			if (!key) return this.data;
			if (!this.data[key]) {
				t.load(key);
				this.data[key] = SQ.AjaxSocket.options.pending;
			}
			return this.data[key];
		}
	}));
	this.graft('files', worker);
}, {
	arguments: ['source'],
	defaults: [''],
	load: function( key ) {
		var t = this;
		if (!key) return;
		var url = t.source.get() + key;
		$.ajax({
			url:  url,
			success: function( data ) {
				if (data) t.put('files.' + key, data);
			},
			error: function( a, b, info ) {
				console.log(a, b, info, a.responseText);
				t.put('files.' + key, SQ.AjaxSocket.options.error);
			}
		});
	},
	traits: 'AjaxLoader Control'
});
SQ.AjaxSocket.options = {
	pending: "Loading...",
	error:  "Sorry, this page is not available at the moment. Please try again later."
};