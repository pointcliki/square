SQ.Closure = SQ.make(function() {
	// Super constructor
	this.DataPath.apply(this, arguments);
}, {
	execute: function( scope ) {
		return SQ(this.data, scope);
	},
	traits: 'Closure Html'
});