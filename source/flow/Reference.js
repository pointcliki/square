SQ.Reference = SQ.make(function() {
	// Super Constructor
	this.Html.apply(this, arguments);
	// Bind changes in argument
	for (var i in arguments) {
		this[i].change(function() {
			this.updateKey();
		});
	}
	this.scope = SQ.scope.path;
	this.model = SQ.model;
	this.path = this.model;
	this.updateKey();
}, {
	updateKey: function() {
		// Unbind old data
		if (this.update) this.path.release(this.key, this.update);
		// Calculate new key
		var key = '', i = 0;
		while (this[i]) {
			key += this[i].get();
			i++;
		}
		this.key = key;
		// Use scope?
		this.path = this.key.startsWith('.') ? (this.key = this.key.substring(1), this.scope) : this.path;
		// Create update if it doesn't exist
		var t = this;
		if (!this.update) {
			this.update = function() {
				var v = t.path.get(t.key);
				// Update html
				if (t.dom) t.dom.html(v || '');
				// Trigger
				t.trigger('', v);
			};
		}
		// Bind to data change
		this.path.change(this.key, this.update);
		// Update now
		this.update();
	},
	generateHtml: function() {
		// Super
		this.Html_generateHtml();
		// Add data
		this.dom.html(this.data || '');
	},
	get: function( value ) {
		return this.path.get(this.key + (value ? '.' + value : ''));
	},
	put: function( key, value ) {
		this.path.put(this.key, value);
	},
	subpath: function( value ) {
		// Get the initial data for 
		var data = this.get(value);
		// Copy over arguments that form the reference key
		var args = [], i = 0;
		while (this[i]) {
			args.push(this[i]);
			i++;
		}
		// Add the next argument which points to our subpath key
		args.push(new SQ.Value(args.length ? '.' + value : value));
		// Create the new instances Structure
		var obj = {data: data, traits: 'Reference'};
		// Apply the constructor
		obj.__proto__ = SQ.Reference.prototype;
		this.construct.apply(obj, args);
		// Set up the instance prototype
		return obj;
	},
	anchor: function() {
		console.warn('Not yet implemented');
	},
	release: function() {
		// Unbind old data
		if (this.update) this.path.release(this.key, this.update);
	},
	traits: 'Reference Html'
});

